These proofs require Coq 8.5.3 and a specific version of Autosubst;
please use [this installation script](coq/installation.sh).

You can then check the proofs as follows:
```
  make _CoqProject
  make -j
```

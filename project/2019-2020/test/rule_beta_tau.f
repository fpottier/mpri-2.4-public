program
      fun [a]: a -> a =
        (fun [a](x : a) = x) [a]

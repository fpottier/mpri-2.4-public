program
      fun [a][b](v: a)(k: b): b =
        let x : a = v in k

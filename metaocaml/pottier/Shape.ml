type ('a, 'stream) shape =
  | Nil
  | Cons of 'a * 'stream

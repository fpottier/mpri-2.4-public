(* The square function. *)

let square x =
  x * x
